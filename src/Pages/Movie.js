import React, { useState, useEffect } from "react";
import { Layout, Space, Card, Col, Row } from "antd";
import mockMovies from "./../services/movies.json";

const { Content } = Layout;

const contentStyle = {
  textAlign: "center",
  minHeight: 120,
  lineHeight: "120px",
  color: "#fff",
  backgroundColor: "#108ee9",
};

const gutterBox = {
  marginTop: "8px",
};

function Movie() {
  const [movies, setMovies] = useState([]);
  useEffect(() => {
    //Runs only on the first render
    setMovies(mockMovies);
  }, []);
  return (
    <Space direction="vertical" style={{ width: "100%" }} size={[0, 48]}>
      <Layout>
        <Content style={contentStyle}>
          <Row gutter={16}>
            {movies.map((el, index) => {
              return (
                <Col style={gutterBox} span={6} key={index}>
                  <Card title={el.name} bordered={false}>
                    <p>Rating: {el.rating}</p>
                    <p>Release Date: {el.releaseDate}</p>
                  </Card>
                </Col>
              );
            })}
          </Row>
        </Content>
      </Layout>
    </Space>
  );
}

export default Movie;
