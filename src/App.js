import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";
import Movie from "./Pages/Movie";

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route exact path="/" element={<Movie />}></Route>
          <Route exact path="/movies" element={<Movie />}></Route>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
